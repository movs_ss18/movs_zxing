package com.example.steffen.movs_zxing;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.content.ContentValues.TAG;

/**
 * Created by Steffen on 20.04.2017.
 */

public class SimpleScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);                // Set the scanner view as the content view
        
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        String rawText =  rawResult.getText();
        Log.v(TAG, rawText); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
        Toast.makeText(this, rawText, Toast.LENGTH_SHORT).show();


        if(URLUtil.isValidUrl(rawText)) { //Für URLS
            Intent sharingIntent= new Intent(Intent.ACTION_VIEW);
            sharingIntent.setData(Uri.parse(rawText));
            startActivity(Intent.createChooser(sharingIntent, "Open with")); //Chooser zum Wählen des Browsers
        } else {  // Für alles was keine Url ist
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "MOVS ZXing");
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, rawText);
            startActivity(Intent.createChooser(sharingIntent, "Open with")); //Chooser zum Wählen wie zu verfahren ist
        }




        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
    }
}
