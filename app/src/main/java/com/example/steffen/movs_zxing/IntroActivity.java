package com.example.steffen.movs_zxing;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

/**
 * Created by Steffen on 20.04.2017.
 */

public class IntroActivity extends AppIntro {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("We need Camera permission", "We want to take some pictures", R.drawable.ic_skip_white, Color.rgb(0, 0, 100)));
        addSlide(AppIntroFragment.newInstance("Thank you", "We will take some pictures", R.drawable.ic_skip_white, Color.rgb(0, 0, 100)));
        askForPermissions(new String[]{Manifest.permission.CAMERA}, 1);


    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        sendResult();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        sendResult();
    }

    public void sendResult(){
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

}
