package com.example.steffen.movs_zxing;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean granted = (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if (!granted) {
            Intent intent = new Intent(this, IntroActivity.class);
            startActivityForResult(intent, 1);
        } else {
            startScanner();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            boolean granted = (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

            if (!granted) {
                Toast.makeText(this, "Camera permission needed", Toast.LENGTH_SHORT).show();
            } else {
                startScanner();
            }
        }
    }

    public void startScanner(){
        Intent intent = new Intent(this, SimpleScannerActivity.class);
        startActivity(intent);
    }
}
